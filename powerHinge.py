import cadquery as cq
from Helpers import show
from math import sin, cos, pi

def hinge(hinge_d, axis_d, hinge_l, axis_l):
    return cq.Workplane("front").circle(hinge_d / 2).extrude(hinge_l / 2, both=True)\
    .circle(axis_d / 2).extrude(axis_l / 2, both=True)

def holder(dev_w, dev_h, hold_w, hold_h):
    pts = [(0, dev_w / 2), (dev_h + 2 * hold_h, dev_w / 2),
           (dev_h + 2 * hold_h, dev_w / 2 - 2 * hold_h),
           (dev_h + hold_h, dev_w / 2 - 2 * hold_h),
           (dev_h + hold_h, dev_w / 2 - hold_h),
           (hold_h, dev_w / 2 - hold_h), (hold_h, 0)]

    return cq.Workplane("front")\
        .polyline(pts).mirrorX()\
        .extrude(hold_w).edges("|Z").fillet(2)\
        .translate((0, dev_w / 2, -hold_w / 2))\

# Non functional dimensions
hold_h = 6.0
mount_play = 0.5
z_fillet = 0.5

# tablet holder
tab_w = 121.2
tab_h = 9.9
tab_hold_w = 18.0
tab_block_d = 3.2

# keyboard holder
kbd_w = 125.3
kbd_h = 16.5
kbd_hold_w = 34.0
kbd_block_d = 5.0

# power hinge
hinge_d = kbd_h + 2 * hold_h
axis_d = 13.0
hinge_l = tab_hold_w
axis_l = kbd_hold_w

tab_hold = holder(tab_w, tab_h, tab_hold_w, hold_h)\
    .translate((0, kbd_h / 2 + hold_h + 2, 0))\

tab_hinge = hinge(hinge_d, axis_d, hinge_l, axis_l)

pt_1_x, pt_1_y, pt_1_z = tab_hold.edges("<X").vertices("<Y").val().toTuple()
pt_2_x, pt_2_y, pt_2_z = tab_hold.edges("<Y").vertices(">X").val().toTuple()
tab_link = cq.Workplane("front").moveTo(pt_1_x, pt_1_y).vLine(-5).line(14.8, -12.9)\
    .lineTo(pt_2_x + 2 * sin(pi/3), pt_2_y + 2 - 2 * cos(pi/3)).close()\
    .extrude(tab_hold_w).translate((0, 0, -tab_hold_w / 2))

tab_block = cq.Workplane("front").polygon(30, hinge_d - 1.8, forConstruction=True)\
    .vertices(cq.selectors.BoxSelector((5, 100, -1), (100, -6, 1))).circle(tab_block_d / 2).extrude(hinge_l / 2, both=True)\
    .rotate((0, 0, 0), (0, 0, 1), -56)

tab_block = cq.Workplane("front").polygon(30, hinge_d - 1.8, forConstruction=True)\
    .vertices(cq.selectors.BoxSelector((13, 100, -1), (100, -1, 1))).circle(tab_block_d / 2).extrude(hinge_l / 2, both=True)\
    .rotate((0, 0, 0), (0, 0, 1), -263).union(tab_block)

tab_hold = tab_hold.union(tab_link).union(tab_hinge).union(tab_block)\
        .rotate((0, 0, 0), (0, 0, 1), 120)\
        .faces(">Z[-1]").fillet(z_fillet).faces(">Z[-2]").fillet(z_fillet)\
        .faces(">Z[-4]").fillet(z_fillet).faces("<Z").fillet(z_fillet)\

kbd_hold = holder(kbd_w, kbd_h, kbd_hold_w, hold_h)\
    .mirror(mirrorPlane='XZ')\
    .translate((0, -(kbd_h / 2 + hold_h + 2), 0))\
    .rotate((0,0,0),(0,0,1), 90)

kbd_hinge_part = cq.Workplane("front").circle(hinge_d / 2)\
    .extrude((axis_l - (hinge_l + 2 * mount_play)) / 2)\
    .faces(">Z").workplane().hole(axis_d + 2 * mount_play)\
    .translate((0, 0, -(axis_l - (hinge_l + 2 * mount_play)) / 4))

pt_1_x, pt_1_y, pt_1_z = kbd_hold.edges("<Y").vertices("<X").val().toTuple()
pt_2_x, pt_2_y, pt_2_z = kbd_hold.edges("<X").vertices(">Y").val().toTuple()
kbd_link_part = cq.Workplane("front").moveTo(pt_1_x, pt_1_y).lineTo(hinge_d / 2 * sin(2.85 * pi / 4), hinge_d / 2 * cos(2.85 * pi / 4)).line(-2,14).lineTo(hinge_d / 2 * sin(-pi / 6), hinge_d / 2 * cos(-pi / 6))\
    .lineTo(pt_2_x + 2 - 2 * sin(pi/4), pt_2_y + 2 * cos(pi/4)).close()\
    .extrude((axis_l - (hinge_l + 2 * mount_play)) / 2)\
    .translate((0, 0, -(axis_l - (hinge_l + 2 * mount_play)) / 4))

kbd_link_hinge_part = kbd_hinge_part.union(kbd_link_part)

kbd_block_x = kbd_hold.vertices("<X").val().toTuple()[0]
kbd_block = cq.Workplane("front").circle(kbd_block_d / 2)\
    .extrude((hinge_l + 2 * mount_play) / 2, both=True)\
    .translate((kbd_block_x + 0.4, 4, 0))

kbd_hold = kbd_hold.union(kbd_link_hinge_part.translate((0, 0, axis_l / 2 - ((axis_l - (hinge_l + 2 * mount_play)) / 4))).faces("<Z").fillet(z_fillet))\
    .union(kbd_link_hinge_part.translate((0, 0, -(axis_l / 2 - ((axis_l - (hinge_l + 2 * mount_play)) / 4)))).faces(">Z").fillet(z_fillet))\
    .union(kbd_block)\
    .faces(">Z").fillet(z_fillet).faces("<Z").fillet(z_fillet)

tab_hold.cut(cq.Workplane("front").box(200, 100, 100).translate((-133, 0, 0)).rotate((0, 0, 0), (0, 0, 1), 30))
kbd_hold.cut(cq.Workplane("front").box(200, 100, 100).translate((133, 0, 0)))

# Render the solid
show(tab_hold)
show(kbd_hold)
# show(kbd_block)
# show(tab_block)
